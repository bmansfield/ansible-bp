# Ansible Playbook Boilerplate

Ansible playbook boilerplate

WARNING! This is very much a WIP and slapped together at the moment. Much is
left to be done


## About

This is by no means meant to be a working example. It is intended to be something
I can `cp -r ansible-bp <my-new-project>` and quickly rip out the parts I need
and have enough of examples for each common type of component that 99% of my
ansible playbooks usually have/need so that I don't have to go writing them from
scratch every time or google each part again and again.


## Future Feature Improvements Targets And Goals

* make better roles
* better examples of inventory, including dynamic inventory for aws
* better instructions on how to setup a new playbook, `pip` or `poetry` commands, etc
* make more targeted and intelligent setups (i.e. a basic provision, a web server, one for a debian and one for a rhel, etc)
* split up some tasks like a docker specific one, maybe a devops tools setup to install common devops tools like aws cli and terraform, etc
* try to make at least one example of all common tasks (i.e. example of a loop, example of a template, example of a file copy, user add, restart and notify for service, etc)
* an example of how to update all packages on all hosts
* add kubernetes cli package

