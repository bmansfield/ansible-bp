INVENTORY := ansible_hosts
USER := svc_acct_001
PLAYBOOK := playbook.yml


.PHONY: build install run
build install run:
	ansible-playbook \
		-i $(INVENTORY) \
		-u $(USER) \
		$(PLAYBOOK)
		# -K  if you want to run as admin and be prompted for password

.PHONY: ls list
ls list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

